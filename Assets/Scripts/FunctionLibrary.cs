using UnityEngine;
using static UnityEngine.Mathf;

public static class FunctionLibrary {

    public delegate Vector3 Function (float u, float v, float t);

    public enum FunctionName { Wave, MultiWave, Ripple, Circle, Cylinder, Sphere, ScalingSphere, VerticalBandedSphere, HorizontalBandedSphere, TwistingSphere, Torus, TwistingTorus }

    static Function[] functions = { Wave, MultiWave, Ripple, Circle, Cylinder, Sphere, ScalingSphere, VerticalBandedSphere, HorizontalBandedSphere, TwistingSphere, Torus, TwistingTorus };

    public static Function GetFunction (FunctionName name) {
		return functions[(int)name];
	}

    public static FunctionName GetNextFunctionName(FunctionName function) {
        return (int) function + 1 >= functions.Length ? 0 : function + 1;
    }

    public static FunctionName GetRandomFunctionNameOtherThan (FunctionName name) {
		var choice = (FunctionName)Random.Range(1, functions.Length);
		return choice == name ? 0 : choice;
	}

    public static Vector3 Wave (float u, float v, float t) {
		return new Vector3(u, Sin(PI * (u + v + t)), v);
	}

    public static Vector3 MultiWave (float u, float v, float t) {
		float y = Sin(PI * (u + 0.5f * t));
        y += Sin(PI * (u + v + 0.25f * t));
		return new Vector3(u, y * (1f / 2.5f), v);
	}

    public static Vector3 Ripple (float u, float v, float t) {
		float d = Sqrt(u * u + v * v);
		float y = Sin(PI * (4f * d - t));
		return new Vector3(u, y / (1f + 10f * d), v);
	}

    public static Vector3 Circle (float u, float v, float t) {
		return new Vector3(Sin(PI * u), 0f, Cos(PI * u));
	}

    public static Vector3 Cylinder (float u, float v, float t) {
		return new Vector3(Sin(PI * u), v, Cos(PI * u));
	}

    public static Vector3 Sphere (float u, float v, float t) {
        float r = Cos(0.5f * PI * v);
		return new Vector3(r * Sin(PI * u), Sin(PI * 0.5f * v), r * Cos(PI * u));
	}

    public static Vector3 ScalingSphere (float u, float v, float t) {
        float r = 0.5f + 0.5f * Sin(PI * t);
        float s = r * Cos(0.5f * PI * v);
        Vector3 p;
        p.x = s * Sin(PI * u);
        p.y = r * Sin(0.5f * PI * v);
        p.z = s * Cos(PI * u);
        return p;
	}

    public static Vector3 VerticalBandedSphere (float u, float v, float t) {
        float r = 0.9f + 0.1f * Sin(8f * PI * u);
		float s = r * Cos(0.5f * PI * v);
        Vector3 p;
        p.x = s * Sin(PI * u);
        p.y = r * Sin(0.5f * PI * v);
        p.z = s * Cos(PI * u);
        return p;
	}

    public static Vector3 HorizontalBandedSphere (float u, float v, float t) {
        float r = 0.9f + 0.1f * Sin(8f * PI * v);
		float s = r * Cos(0.5f * PI * v);
        Vector3 p;
        p.x = s * Sin(PI * u);
        p.y = r * Sin(0.5f * PI * v);
        p.z = s * Cos(PI * u);
        return p;
	}

    public static Vector3 TwistingSphere (float u, float v, float t) {
        float r = 0.9f + 0.1f * Sin(PI * (6f * u + 4f * v + t));
		float s = r * Cos(0.5f * PI * v);
        Vector3 p;
        p.x = s * Sin(PI * u);
        p.y = r * Sin(0.5f * PI * v);
        p.z = s * Cos(PI * u);
        return p;
	}

    public static Vector3 Torus (float u, float v, float t) {
		float r1 = 0.75f;
		float r2 = 0.25f;
		float s = r1 + r2 * Cos(PI * v);
		Vector3 p;
		p.x = s * Sin(PI * u);
		p.y = r2 * Sin(PI * v);
		p.z = s * Cos(PI * u);
		return p;
	}

    public static Vector3 TwistingTorus (float u, float v, float t) {
		float r1 = 0.7f + 0.1f * Sin(PI * (6f * u + 0.5f * t));
		float r2 = 0.15f + 0.05f * Sin(PI * (8f * u + 4f * v + 2f * t));
		float s = r1 + r2 * Cos(PI * v);
		Vector3 p;
		p.x = s * Sin(PI * u);
		p.y = r2 * Sin(PI * v);
		p.z = s * Cos(PI * u);
		return p;
	}

    public static Vector3 Morph (
		float u, float v, float t, Function from, Function to, float progress
	) {
        return Vector3.LerpUnclamped(
			from(u, v, t), to(u, v, t), SmoothStep(0f, 1f, progress)
		);
    }
}